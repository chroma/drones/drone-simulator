#!/usr/bin/env python
# coding: utf-8

# Auteur : vledoze
# Date   : 01/10/2018

# Systemes de coordonées:
#   O      : point de départ du drone
#   M(t)   : centre du drone à l'instant t
#   ENU    : Repere fixe East-North-Up centré sur le point O de départ du drone.
#            M(t)_ENU = (pe, pn, pu)
#   FLUO   : Repere fixe Front-Left-Up centré sur le point M(t=0)=O de départ du drone.
#            M(t)_FLU0 = (px, py, pz)
#   FLU1   : Repere fixe Front-Left-Up centré sur le point M(t)  à chaque instant, avec le plan (px, py) // p(x1, py1)
#            M(t)_FLU1 = (px1, py1, pz)
#   FLU(t) : Repère mobile Front-Left-Up centré sur le point M(t) à chaque instant.
#            M(t)_FLU(t) = (pa, pb, pc)
#   IJK    : Repère mobile decrivant les axes de mesures d'un capteur quelconque, centré sur lui-même.
#            Repere IJK imu != repere IJK magnetometre
#
# Etats du drone
#   (px, py, pz)     : Position de M(t) exprimé dans le repere FLU0
#   (va, vb, vc)     : Vitesse lineaire de M(t) exprimé dans le repere FLU(t)
#   (qi, qj, qk, qw) : Quaternion exprimant l'orientation du repère FLU(t) dans le repère FLU0
#   (wa, wb, wc)     : Vitesse angulaire de M(t) exprimé dans le repere FLU(t)
#
# Mesures du drone
#   (pe, pn)         : Mesure de la position du point M(t) projetée sur le plan East-North du repère ENU
#   (px, py)         : Mesure de la position du point M(t) projetée sur le plan Front-Left du repère FLU0
#   (pu)             : Mesure de la position du point M(t) projetée sur l'axe Up du repère ENU
#   (pz)             : Mesure de la position du point M(t) projetée sur l'axe Up du repère FLU0
#   (pc)             : Mesure de la position du point M(t) projetée sur l'axe Up du repère FLU(t)
#   (vx, vy, vz)     : Mesure de la vitesse du point M(t) dans le repère FLU0
#   (ve, vn)         : Mesure de la vitesse du point M(t) projetée sur le plan East-North du repère ENU
#   (va, vb)         : Mesure de la vitesse du point M(t) projetée sur le plan Front-Left du repère FLU(t)
#   (qi, qj, qk, qw) : Quaternion exprimant l'orientation du repère FLU(t) dans le repère FLU0

# ##IMPORTATIONS =======================================================
# Import lib standard
import rospy
import math
import numpy

# Import lib locale
from math_utils import rotatewithquaternion
from math_utils import quaternioninv

# Messages ROS
from nav_msgs.msg import Odometry as Msg_Odometry
from geometry_msgs.msg import Point as Msg_Point
from geometry_msgs.msg import Wrench as Msg_Wrench

# Services ROS
from std_srvs.srv import Trigger as Srv_Trigger
from std_srvs.srv import TriggerResponse as Srv_TriggerResponse

# ##CONSTANTES =========================================================
# Parametres ROS par defaut
DEFAULT_FREQ = 10.0

# Constantes
G = 9.81


# ##CLASSES ============================================================
class PioneerPilot(object):
    """
    """

    # Fonctions initialisation -----------------------------------------
    def __init__(self):
        """
        """
        # Initialisation des parametres
        self.__init__parameters()
        # Vecteur d'état
        self.state_ok = False
        self.state_pxyz = None
        self.state_quat = None
        self.state_vxyz = None
        self.state_omgz = None
        # Target
        self.target_pxyz = None
        self.target_list = []
        # Historique Commandes
        self.cmd_tet_prev = 0.0
        self.cmd_phi_prev = 0.0
        # Creation des publishers ROS
        self.__init__publishers()
        # Souscription aux topics ROS
        self.__init__subscribers()
        # Creation des services
        self.__init__services_provided()

    def __init__parameters(self):
        """
        Initialisation des parametres ROS
        """
        self.freq = rospy.get_param("~freq", DEFAULT_FREQ)

    def __init__publishers(self):
        """
        Initialisation du publisher ROS
        """
        self.pub_cmd = rospy.Publisher("~cmd", Msg_Wrench, queue_size=10)

    def __init__subscribers(self):
        """
        Initialisation des subscriber ROS
        """
        rospy.Subscriber("topic_odometry", Msg_Odometry, self.callback_odometry)
        rospy.Subscriber("topic_target", Msg_Point, self.callback_target)

    def __init__services_provided(self):
        """
        Initialisation des services ROS
        """
        rospy.Service('~reset_targets', Srv_Trigger, self.service_reset_targets)

    # Fonctions publiques ----------------------------------------------
    def run(self):
        """
        """
        rate = rospy.Rate(self.freq)
        while not rospy.is_shutdown():
            rate.sleep()
            self.step()

    def step(self):
        """
        """
        # Seulement si OK state
        if self.state_ok:
            # Choix de la cible
            self.update_target()
            # Calcul des commmandes
            self.compute_cmd()

    def update_target(self):
        # Nouvelle cible disponible
        if len(self.target_list) > 0:
            # Si on avait deja une cible
            if (self.target_pxyz is not None):
                # Distance avec la cible courante
                dist_pxyz = numpy.linalg.norm(self.state_pxyz - self.target_pxyz)
                # Test si on a atteint la cible courante
                if (dist_pxyz < 0.5):
                    # Passage a la cible suivante
                    target = self.target_list.pop(0)
                    self.target_pxyz = numpy.array([target.x, target.y, target.z])
            # Si on avait aucune cible
            else:
                # Premiere cible
                target = self.target_list.pop(0)
                self.target_pxyz = numpy.array([target.x, target.y, target.z])

    def compute_cmd(self):
        """
        Calcule de la commande
        """
        # Si on a une cible
        if (self.target_pxyz is not None):
            # Commande lineaire
            cmd_force_xyz = (self.target_pxyz - self.state_pxyz) - 2.0*self.state_vxyz
            cmd_force_abc = rotatewithquaternion(cmd_force_xyz, quaternioninv(self.state_quat))
            cmd_force_fwd = max(0.0, cmd_force_abc[0])
            # Commande angulaire
            if math.sqrt(cmd_force_xyz[0]**2 + cmd_force_xyz[1]**2) > 0.5:
                cmd_torque_z = math.atan2(cmd_force_abc[1], cmd_force_abc[0]) - 2.0*self.state_omgz
            else:
                cmd_torque_z = 0.0
            # Creation Message ROS avec les commandes
            msg = Msg_Wrench()
            msg.force.x = 5.0*math.tanh(cmd_force_fwd)
            msg.force.y = 0.0
            msg.force.z = 5.0*math.tanh(cmd_force_abc[2])
            msg.torque.x = 0.0
            msg.torque.y = 0.0
            msg.torque.z = 2.0*math.tanh(cmd_torque_z)
            # Publication Message ROS avec les commandes
            self.pub_cmd.publish(msg)

    # Fonctions callback -----------------------------------------------
    def callback_odometry(self, data):
        """
        """
        # Position dans repere ENU
        self.state_pxyz = numpy.array([
            data.pose.pose.position.x,
            data.pose.pose.position.y,
            data.pose.pose.position.z])
        # Orientation repere FLU(t) / ENU
        self.state_quat = [
            data.pose.pose.orientation.x,
            data.pose.pose.orientation.y,
            data.pose.pose.orientation.z,
            data.pose.pose.orientation.w]
        # Vitesse lineaire dans repère ENU
        self.state_vxyz = rotatewithquaternion(
            numpy.array([
                data.twist.twist.linear.x,
                data.twist.twist.linear.y,
                data.twist.twist.linear.z]),
            self.state_quat)
        # Vitesse angulaire verticale dans repère FLU
        self.state_omgz = data.twist.twist.angular.z
        # OK state
        self.state_ok = True

    def callback_target(self, data):
        """
        """
        self.target_list.append(data)

    def service_reset_targets(self, req):
        """
        """
        # Preparation reponse
        ret = Srv_TriggerResponse()
        # NOK state
        if not self.state_ok:
            ret.success = False
            return ret
        # OK state
        else:
            # On stoppe l'avancée
            self.target_pxyz = self.state_pxyz
            # RAZ de la liste des cibles
            self.target_list = []
            # Sortie
            ret.success = True
            return ret

    # Fonctions privée -------------------------------------------------

# ##FONCTIONS ==========================================================


# ##MAIN ===============================================================
if __name__ == "__main__":
    rospy.init_node("pilot", anonymous=False)
    pilote = PioneerPilot()
    pilote.run()
