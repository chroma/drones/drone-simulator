#!/usr/bin/env python
# coding: utf-8

# Auteur : vledoze
# Date   : 01/10/2018
# Auteur 2: vdufour
# Date    : 29/03/2021

# Systemes de coordonées:
#   O      : point de départ du drone
#   M(t)   : centre du drone à l'instant t
#   ENU    : Repere fixe East-North-Up centré sur le point O de départ du drone.
#            M(t)_ENU = (pe, pn, pu)
#   FLUO   : Repere fixe Front-Left-Up centré sur le point M(t=0)=O de départ du drone.
#            M(t)_FLU0 = (px, py, pz)
#   FLU1   : Repere fixe Front-Left-Up centré sur le point M(t) à chaque instant, avec le plan (px, py) // p(x1, py1)
#            M(t)_FLU1 = (px1, py1, pz)
#   FLU(t) : Repère mobile Front-Left-Up centré sur le point M(t) à chaque instant.
#            M(t)_FLU(t) = (pa, pb, pc)
#   IJK    : Repère mobile decrivant les axes de mesures d'un capteur quelconque, centré sur lui-même.
#            Repere IJK imu != repere IJK magnetometre
#
# Etats du drone
#   (px, py, pz)     : Position de M(t) exprimé dans le repere FLU0
#   (va, vb, vc)     : Vitesse lineaire de M(t) exprimé dans le repere FLU(t)
#   (qi, qj, qk, qw) : Quaternion exprimant l'orientation du repère FLU(t) dans le repère FLU0
#   (wa, wb, wc)     : Vitesse angulaire de M(t) exprimé dans le repere FLU(t)
#
# Mesures du drone
#   (pe, pn)         : Mesure de la position du point M(t) projetée sur le plan East-North du repère ENU
#   (px, py)         : Mesure de la position du point M(t) projetée sur le plan Front-Left du repère FLU0
#   (pu)             : Mesure de la position du point M(t) projetée sur l'axe Up du repère ENU
#   (pz)             : Mesure de la position du point M(t) projetée sur l'axe Up du repère FLU0
#   (pc)             : Mesure de la position du point M(t) projetée sur l'axe Up du repère FLU(t)
#   (vx, vy, vz)     : Mesure de la vitesse du point M(t) dans le repère FLU0
#   (ve, vn)         : Mesure de la vitesse du point M(t) projetée sur le plan East-North du repère ENU
#   (va, vb)         : Mesure de la vitesse du point M(t) projetée sur le plan Front-Left du repère FLU(t)
#   (qi, qj, qk, qw) : Quaternion exprimant l'orientation du repère FLU(t) dans le repère FLU0

# ##IMPORTATIONS =======================================================
# Import lib standard
import rospy
import math
import numpy
import time

# Import lib locale
from math_utils import D2R
from math_utils import euler2quaternion
from math_utils import quaternion2euler
from math_utils import quaternion2psi
from math_utils import quaternioninv
from math_utils import rotatewithpsi
from math_utils import rotatewithquaternion

# Messages ROS
from std_msgs.msg import Empty as Msg_Empty
from sensor_msgs.msg import Imu as Msg_Imu
from nav_msgs.msg import Odometry as Msg_Odometry
from geometry_msgs.msg import Pose as Msg_Pose

# Services ROS
from std_srvs.srv import Trigger as Srv_Trigger
from std_srvs.srv import TriggerResponse as Srv_TriggerResponse

# ##CONSTANTES =========================================================
# Parametres ROS par defaut
DEFAULT_FREQ = 10.0
DEFAULT_BORNE_ACC_XY = 0.5
DEFAULT_BORNE_ACC_Z = 0.5
DEFAULT_TARGET_PX = 0.0
DEFAULT_TARGET_PY = 0.0
DEFAULT_TARGET_PZ = 2.0

# Constantes
G = 9.81


# ##CLASSES ============================================================
class DronePilot(object):
    """
    """

    # Fonctions initialisation -----------------------------------------
    def __init__(self):
        """
        """
        # Initialisation des parametres
        self.__init__parameters()
        # Vecteur d'état
        self.state_ok = False
        self.state_pxyz = numpy.array([0.0, 0.0, 0.0])
        self.state_quat = numpy.array([0.0, 0.0, 0.0, 1.0])
        self.state_vabc = numpy.array([0.0, 0.0, 0.0])
        # Target
        self.target_pxyz = numpy.array([self.target_px, self.target_py, self.target_pz])
        self.target_vxyz = numpy.array([0.0, 0.0, 0.0])
        self.target_axyz = numpy.array([0.0, 0.0, 0.0])
        self.target_psi = 0.0
        self.target_dpsi = 0.0
        self.target_list = []
        self.dist_pxyz = 0
        self.dist_psi = 0
        self.last_max = numpy.array([0.0, 0.0, 0.0])
        self.sign_dir = numpy.array([0.0, 0.0, 0.0])
        # Historique Commandes
        self.cmd_tet_prev = 0.0
        self.cmd_phi_prev = 0.0
        # Creation des publishers ROS
        self.__init__publishers()
        # Souscription aux topics ROS
        self.__init__subscribers()
        # Creation des services
        self.__init__services_provided()

    def __init__parameters(self):
        """
        Initialisation des parametres ROS
        """
        self.freq = rospy.get_param("~freq", DEFAULT_FREQ)
        self.borne_acc_xy = rospy.get_param("~borne_acc_xy", DEFAULT_BORNE_ACC_XY)
        self.borne_acc_z = rospy.get_param("~borne_acc_z", DEFAULT_BORNE_ACC_Z)
        self.target_px = rospy.get_param("~target_px", DEFAULT_TARGET_PX)
        self.target_py = rospy.get_param("~target_py", DEFAULT_TARGET_PY)
        self.target_pz = rospy.get_param("~target_pz", DEFAULT_TARGET_PZ)
    def __init__publishers(self):
        """
        Initialisation du publisher ROS
        """
        self.pub_cmd = rospy.Publisher("~cmd", Msg_Imu, queue_size=100)
        self.pub_home = rospy.Publisher("~back_to_home", Msg_Empty, queue_size=10)

    def __init__subscribers(self):
        """
        Initialisation des subscriber ROS
        """
        rospy.Subscriber("topic_odometry", Msg_Odometry, self.callback_odometry)
        rospy.Subscriber("topic_target", Msg_Pose, self.callback_target)

    def __init__services_provided(self):
        """
        Initialisation des services ROS
        """
        rospy.Service('~reset_targets', Srv_Trigger, self.service_reset_targets)

    # Fonctions publiques ----------------------------------------------
    def run(self):
        """
        """
        rate = rospy.Rate(self.freq)
        while not rospy.is_shutdown():
            rate.sleep()
            self.step()

    def get_max_cmd(self,cmd_axyz):
        error_xyz = self.target_pxyz - self.state_pxyz
        self.sign_dir = numpy.sign([math.floor(x) for x in error_xyz])
        if self.dist_pxyz > 1.5:
            cmd_axyz = numpy.array([max(abs(cmd_axyz[0]),abs(self.last_max[0]))*self.sign_dir[0],max(abs(cmd_axyz[1]),abs(self.last_max[1]))*self.sign_dir[1],cmd_axyz[2]])
            self.last_max = cmd_axyz
        return cmd_axyz


    def step(self):
        """
        """
        # Seulement si OK state
        if self.state_ok:
            # Choix de la cible
            self.update_target()
            # Calcul des commmandes
            self.last_max = self.compute_cmd()

    def update_target(self):
        # Nouvelle cible disponible
        if len(self.target_list) > 0:
            # Distance avec la cible courante
            self.dist_pxyz = numpy.linalg.norm(self.state_pxyz - self.target_pxyz)
            # Distance avec le cap visé
            self.dist_psi = 2.0*math.atan(math.tan(0.5*(self.target_psi - quaternion2psi(self.state_quat))))

            if (self.dist_pxyz < 1.5) & (self.dist_psi < 5.0*D2R):
                # Passage à la cible suivante
                target = self.target_list.pop(0)
                self.target_pxyz = numpy.array([
                    target.position.x,
                    target.position.y,
                    target.position.z])
                self.target_psi = quaternion2psi([
                    target.orientation.x,
                    target.orientation.y,
                    target.orientation.z,
                    target.orientation.w])
                self.last_max = numpy.array([0.0,0.0,0.0])

    def compute_cmd(self):
        """
        Calcul la commande à partir de la recherche d'un point d'équilibre
        entre Xt la position cible et X la position courante

            (Xt - X)'' + 2(Xt - X) + (Xt - X) = 0
        =>   X'' = Xt'' + 2(Xt - X) + (Xt - X)

        avec X'' = R1(psi)*A + R2(psi)*V*w
        avec R1 et R2 des matrices exprimées en fonction du cap actuel
        avec V : la vitesse actuelle exprimée dans le repère drone
        avec A : la commande en acceleration exprimée dans le repère drone
        avec w : la vitesse de rotation autour de l'axe vertical du repère drone

        Equivalence angle et acceleration calculée avec
        m.A =  |  0    + T*| cos(phi).sin(tet)
               |  0        |-sin(tet)
               | -m*G      | cos(phi).cos(tet)
        Avec T la poussée (N)
        et A les accélérations commandées dans le repere drone Ruvw
        """
        # Attitude courrante
        state_psi, state_tet, state_phi = quaternion2euler(self.state_quat)
        # Erreurs en Position
        error_xyz = self.target_pxyz - self.state_pxyz
        # Erreur en Vitesse lineaire
        error_vxyz = self.target_vxyz - rotatewithquaternion(self.state_vabc, self.state_quat)


        # Commandes en Accelerations dans le repere FLU0
        cmd_axyz = self.target_axyz + 2.0*error_vxyz + error_xyz
        #Calcul commande selon bornes
        cmd_axyz[0] = max(-self.borne_acc_xy, min(self.borne_acc_xy, cmd_axyz[0]))  # Bornes sur accelerations realisables ~ maximum tilt
        cmd_axyz[1] = max(-self.borne_acc_xy, min(self.borne_acc_xy, cmd_axyz[1]))  # Bornes sur accelerations realisables ~ maximum tilt
        cmd_axyz[2] = max(-self.borne_acc_z, min(self.borne_acc_z, cmd_axyz[2]))    # Bornes sur accelerations realisables ~ securité pour descendre
        cmd_axyz[2] += G
        #print("Cmd Res: {}".format(cmd_axyz))
        # Choix garder commande max ou changer si on a déjà une cible suivante
        if len(self.target_list) > 0:
            cmd_axyz = self.get_max_cmd(cmd_axyz)
        #print("Cmd Res: {}".format(cmd_axyz))
        # Commandes en Accéleration dans le repère FLU(t)
        cmd_ax1y1z = rotatewithpsi(cmd_axyz, -state_psi)
        # Commandes en Attitude <=> Commandes en Accéleration
        cmd_tet = math.atan2(cmd_ax1y1z[0], cmd_ax1y1z[2])
        cmd_phi = math.atan2(-cmd_ax1y1z[1], cmd_ax1y1z[2]/math.cos(cmd_tet))
        cmd_qat = euler2quaternion(self.target_psi, cmd_tet, cmd_phi)
        # Commande en Poussée
        cmd_thrust = rotatewithquaternion(cmd_axyz, quaternioninv(self.state_quat))[2]
        # Erreur en Attitude
        err_psi = 2.0*math.atan(math.tan(0.5*(self.target_psi - state_psi)))
        err_tet = cmd_tet - state_tet
        err_phi = cmd_phi - state_phi
        # Derivée commandes en Attitude
        cmd_dtet = (cmd_tet - self.cmd_tet_prev) * self.freq
        cmd_dphi = (cmd_phi - self.cmd_phi_prev) * self.freq
        self.cmd_tet_prev = cmd_tet
        self.cmd_phi_prev = cmd_phi
        # Matrice inverse de Transfert de rotation
        Ti = numpy.ndarray(
             shape=(3, 3),
             dtype=float,
             buffer=numpy.array(
                 [0.0, -math.sin(state_psi),  math.cos(state_tet)*math.cos(state_psi),
                  0.0,  math.cos(state_psi),  math.cos(state_tet)*math.sin(state_psi),
                  1.0,  0.0,                 -math.sin(state_tet)]))
        # Commande en Vitesse angulaire
        cmd_dA = numpy.ndarray(
             shape=(3, 1),
             dtype=float,
             buffer=numpy.array(
                [self.target_dpsi + err_psi,
                 cmd_dtet + err_tet,
                 cmd_dphi + err_phi]))
        cmd_wabc = rotatewithquaternion(numpy.matmul(Ti, cmd_dA).flatten(), quaternioninv(self.state_quat))
        # Creation Message ROS avec les commandes
        msg = Msg_Imu()
        msg.header.stamp = rospy.Time.now()
        msg.orientation.x = cmd_qat[0]
        msg.orientation.y = cmd_qat[1]
        msg.orientation.z = cmd_qat[2]
        msg.orientation.w = cmd_qat[3]
        msg.linear_acceleration.z = cmd_thrust
        msg.angular_velocity.x = cmd_wabc[0]
        msg.angular_velocity.y = cmd_wabc[1]
        msg.angular_velocity.z = cmd_wabc[2]
        # Publication Message ROS avec les commandes
        self.pub_cmd.publish(msg)
        return cmd_axyz


    # Fonctions callback -----------------------------------------------
    def callback_odometry(self, data):
        """
        """
        # Position dans repere ENU
        self.state_pxyz = numpy.array([
            data.pose.pose.position.x,
            data.pose.pose.position.y,
            data.pose.pose.position.z])
        # Orientation repere FLU(t) / ENU
        self.state_quat = [
            data.pose.pose.orientation.x,
            data.pose.pose.orientation.y,
            data.pose.pose.orientation.z,
            data.pose.pose.orientation.w]
        # Vitesse lineaire dans repère FLU(t)
        self.state_vabc = numpy.array([
            data.twist.twist.linear.x,
            data.twist.twist.linear.y,
            data.twist.twist.linear.z])
        # Vitesse angulaire dans repère FLU(t)
        self.state_wabc = numpy.array([
            data.twist.twist.angular.x,
            data.twist.twist.angular.y,
            data.twist.twist.angular.z])
        # OK state
        self.state_ok = True

    def callback_target(self, data):
        """
        """
        self.target_list.append(data)

    def service_reset_targets(self, req):
        """
        """
        # Preparation reponse
        ret = Srv_TriggerResponse()
        # NOK state
        if not self.state_ok:
            ret.success = False
            return ret
        # OK state
        else:
            # On stoppe l'avancée
            self.target_pxyz = self.state_pxyz
            self.target_psi = quaternion2psi(self.state_quat)
            # RAZ de la liste des cibles
            self.target_list = []
            # Sortie
            ret.success = True
            return ret

    # Fonctions privée -------------------------------------------------

# ##FONCTIONS ==========================================================


# ##MAIN ===============================================================
if __name__ == "__main__":
    rospy.init_node("pilot", anonymous=False)
    pilote = DronePilot()
    pilote.run()
