#!/usr/bin/env python
# coding: utf-8

# Auteur : vledoze
# Date   : 25/05/2018

### IMPORTATIONS =======================================================
#Import lib standard
import rospy
import sys

#Import lib locale

#Messages ROS
from std_msgs.msg import Empty as Msg_Empty
from sensor_msgs.msg import Joy as Msg_Joy
from chroma_gazebo_plugins_msgs.msg import Rotorspeed as Msg_Rotorspeed

#Services ROS
from std_srvs.srv import Empty as Srv_Empty

### CONSTANTES =========================================================
#Indices moteurs
FRONT_RIGHT = 0
BACK_LEFT = 1
FRONT_LEFT = 2
BACK_RIGHT = 3

#Constantes Commandes
PITCH_CONSTANT = 0.1
ROLL_CONSTANT = 0.1
YAW_CONSTANT = 0.1

#Boutons
PS3_BUTTON_ACTION_TRIANGLE   = 0
PS3_BUTTON_ACTION_CIRCLE     = 1
PS3_BUTTON_ACTION_CROSS      = 2
PS3_BUTTON_ACTION_SQUARE     = 3
PS3_BUTTON_REAR_LEFT_1       = 4
PS3_BUTTON_REAR_RIGHT_1      = 5
PS3_BUTTON_REAR_LEFT_2       = 6
PS3_BUTTON_REAR_RIGHT_2      = 7
PS3_BUTTON_START             = 9
PS3_BUTTON_SELECT            = 8
PS3_BUTTON_STICK_LEFT        = 10
PS3_BUTTON_STICK_RIGHT       = 11

#Axes
PS3_AXIS_STICK_LEFT_LEFTWARDS    = 0
PS3_AXIS_STICK_LEFT_UPWARDS      = 1
PS3_AXIS_STICK_RIGHT_LEFTWARDS   = 2
PS3_AXIS_STICK_RIGHT_UPWARDS     = 3
PS3_AXIS_BUTTON_CROSS_LEFTWARD   = 4
PS3_AXIS_BUTTON_CROSS_UPWARD     = 5

### CLASSES ============================================================
class Classe(object):
    """
    """

    # Fonctions initialisation -----------------------------------------
    def __init__(self):
        """
        """
        #Valeur vitesses de rotation moteurs
        self.values = [0.0, 0.0, 0.0, 0.0]
        self.armed = False
        #Parametre
        self.rotorspeed_max = rospy.get_param("~rotorspeed_max", 140.0)
        #Creation des publishers ROS
        self.__init__publishers()
        #Souscription aux topics ROS
        self.__init__subscribers()

    def __init__publishers(self):
        """
        Initialisation du publisher ROS
        """
        self.pub_rotorspeed = rospy.Publisher("command/rotorspeed", Msg_Rotorspeed, queue_size=1)

    def __init__subscribers(self):
        """
        Initialisation des subscriber ROS
        """
        rospy.Subscriber("joy", Msg_Joy, self.callback_msg_joy)

    # Fonctions publiques ----------------------------------------------
    def run(self):
        """
        """
        #Boucle temporelle
        rate = rospy.Rate(20) # Commande à 20Hz
        while not rospy.is_shutdown():
            rate.sleep()
            self.step()

    def step(self):
        msg_rotorspeed = Msg_Rotorspeed()
        msg_rotorspeed.values = self.values
        self.pub_rotorspeed.publish(msg_rotorspeed)

    # Fonctions de publication -----------------------------------------

    # Fonctions callback -----------------------------------------------
    def callback_msg_joy(self, data):
        """ Callback sur recpetion message Joystick """
        #
        cmd_avant_arriere = data.axes[PS3_AXIS_STICK_LEFT_UPWARDS]
        cmd_left_right = data.axes[PS3_AXIS_STICK_LEFT_LEFTWARDS]
        cmd_rot = data.axes[PS3_AXIS_STICK_RIGHT_LEFTWARDS]
        cmd_up = data.buttons[PS3_BUTTON_REAR_RIGHT_2]
        cmd_down = data.buttons[PS3_BUTTON_REAR_LEFT_2]
        cmd_arm = data.buttons[PS3_BUTTON_ACTION_CROSS]
        cmd_disarm = data.buttons[PS3_BUTTON_ACTION_SQUARE]
        cmd_reset = data.buttons[PS3_BUTTON_ACTION_CIRCLE]
        #Commandes
        s = "Commandes : \n"
        #
        if cmd_reset > 0.0:
            self._reset_world()
            return
        if (cmd_arm > 0.0) & (not self.armed):
            s += "  Commande ARMEMENT \n"
            val = 76.0 #self.rotorspeed_max/100.0
            self.values = [val, val, val, val]
            self.armed = True
            return
        if (cmd_disarm > 0.0) & (self.armed):
            s += "  Commande DESARMEMENT \n"
            self.values = [0., 0., 0., 0.]
            self.armed = False
            return
        # Commande en poussée
        if cmd_up > 0.0:
            s += "  Commande MONTÉE \n"
            thrust =  self.rotorspeed_max/100.0
        elif cmd_down > 0.0:
            s += "  Commande DESCENTE \n"
            thrust = -self.rotorspeed_max/100.0
        else:
            thrust = 0.0
        #Commande en tangage
        if cmd_avant_arriere > 0.0:
            s += "  Commande AVANT : {0} \n".format(abs(cmd_avant_arriere))
        elif cmd_avant_arriere < 0.0:
            s += "  Commande ARRIERE : {0} \n".format(abs(cmd_avant_arriere))
        else:
            s += "  Commande NULLE AVANT-ARRIERE \n"
        #
        if cmd_left_right > 0.0:
            s += "  Commande LEFT : {0} \n".format(abs(cmd_left_right))
        elif cmd_left_right < 0.0:
            s += "  Commande RIGHT : {0} \n".format(abs(cmd_left_right))
        else:
            s += "  Commande NULLE LEFT-RIGHT \n"
        #
        if cmd_rot > 0.0:
            s += "  Commande ROT. POS. : {0} \n".format(abs(cmd_rot))
        elif cmd_rot < 0.0:
            s += "  Commande ROT. NEG. : {0} \n".format(abs(cmd_rot))
        else:
            s += "  Commande NULLE ROT \n"
        # print self.values
        pitch = cmd_avant_arriere*PITCH_CONSTANT
        roll = cmd_left_right*ROLL_CONSTANT
        yaw = cmd_rot*YAW_CONSTANT
        self.values[FRONT_RIGHT] += thrust - pitch + roll - yaw
        self.values[BACK_LEFT] += thrust + pitch - roll - yaw
        self.values[FRONT_LEFT] += thrust - pitch - roll + yaw
        self.values[BACK_RIGHT] += thrust + pitch + roll + yaw

    # Fonctions privée -------------------------------------------------
    def _reset_world(self):
        """
        Intialisations des services
        """
        rospy.wait_for_service('/gazebo/reset_world')
        try:
            reset_world = rospy.ServiceProxy('/gazebo/reset_world', Srv_Empty)
            reset_world()
            self._reset_attributes()
        except rospy.ServiceException, e:
            print "Service reset_world call failed: %s"%e

    def _reset_attributes(self):
        """
        Re-Initialisation des attributs
        """
        #Valeur vitesses de rotation moteurs
        self.values = [0.0, 0.0, 0.0, 0.0]
        self.armed = False

### FONCTIONS ==========================================================

### MAIN ===============================================================
if __name__=="__main__":
    #Création du noeud
    rospy.init_node("node", anonymous=False)
    node = Classe()
    node.run()
