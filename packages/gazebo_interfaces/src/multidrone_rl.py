#!/usr/bin/env python
# coding: utf-8

# Auteur : vledoze
# Date   : 25/05/2018

### IMPORTATIONS =======================================================
#Import lib standard
import math
import numpy
import random
import rospy
import sys
import time

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F

import matplotlib
import matplotlib.pyplot as plt

from collections import namedtuple
from torch.autograd import Variable

#Import lib locale

#Messages ROS
from std_msgs.msg import Empty as Msg_Empty
from std_msgs.msg import Float32 as Msg_Float32
from sensor_msgs.msg import Imu as Msg_Imu
from chroma_gazebo_plugins_msgs.msg import Rotorspeed as Msg_Rotorspeed
from chroma_gazebo_plugins_msgs.msg import BodyGroundTruth as Msg_BodyGroundTruth

#Services ROS
from std_srvs.srv import Empty as Srv_Empty

### CONSTANTES =========================================================
#Limite de temps d'un episode
EPISODE_STEPS_LIMIT = 100

### CLASSES ============================================================
class MultiDroneReinforcementLearning(object):
    """
    """

    # Fonctions initialisation -----------------------------------------
    def __init__(self):
        """
        """
        #Parametres
        self.__init__parameters()
        #Attributs episodes
        self.__init__episodes_attributes()
        #Souscription aux services ROS
        self.__init__subscribers()

    def __init__parameters(self):
        """
        Attributs qui seront remis à zero à chaque nouvel épisode
        """
        #Type de drone
        self.drone = rospy.get_param("~drone", "crazyflie")
        #Nombre de drones
        self.drone_number = rospy.get_param("~drone_number", 0)

    def __init__episodes_attributes(self):
        """
        Attributs qui seront remis à zero à chaque nouvel épisode
        """
        #Pas de temps par épisode
        self.episode_steps = 0
        self.episode_steps_limit = \
            rospy.get_param("~episode_steps_limit", EPISODE_STEPS_LIMIT)*random.random()

    def __init__subscribers(self):
        """
        Initialisation des services ROS
        """
        #Stockage des services
        self.stop_pub_list = []
        self.start_pub_list = []
        #Souscription
        for i in range(self.drone_number):
            self.stop_pub_list.append(rospy.Publisher('/{0}_{1}/stop_learning'.format(self.drone, i+1), Msg_Empty, queue_size=1))
            self.start_pub_list.append(rospy.Publisher('/{0}_{1}/start_learning'.format(self.drone, i+1), Msg_Empty, queue_size=1))

    # Fonctions publiques ----------------------------------------------
    def run(self):
        """
        """
        #Boucle temporelle
        rate = rospy.Rate(10) # Commande à 10Hz
        while not rospy.is_shutdown():
            rate.sleep()
            self.step()

    def step(self):
        """
        Step
        """
        #Incrementation du pas
        self.episode_steps += 1
        if self.episode_steps > self.episode_steps_limit:
            self._reset_world()

    # Fonctions de publication -----------------------------------------

    # Fonctions callback -----------------------------------------------

    # Fonctions privée -------------------------------------------------
    def _reset_world(self):
        """
        Intialisations des services
        """
        #Stop l'apprentissage des drones
        for stop_pub in self.stop_pub_list:
            stop_pub.publish(Msg_Empty())
        #Reset de gazebo
        rospy.wait_for_service('/gazebo/reset_world')
        try:
            reset_world = rospy.ServiceProxy('/gazebo/reset_world', Srv_Empty)
            reset_world()
        except rospy.ServiceException, e:
            print "Service reset_world call failed: %s"%e
        #On reinitialise les variables d'episode
        self.__init__episodes_attributes()
        #Relance l'apprentissage des drones
        for start_pub in self.start_pub_list:
            start_pub.publish(Msg_Empty())

### FONCTIONS ==========================================================

### MAIN ===============================================================
if __name__=="__main__":
    rospy.init_node("node", anonymous=False)
    node = MultiDroneReinforcementLearning()
    plt.figure()
    node.run()
