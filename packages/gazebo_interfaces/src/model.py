#!/usr/bin/env python
# coding: utf-8

# Auteur : vledoze
# Date   : 25/05/2018

### IMPORTATIONS =======================================================
#Import lib standard
import rospy
import sys

#Import lib locale

#Messages ROS
from std_msgs.msg import Empty as Msg_Empty
from chroma_gazebo_plugins_msgs.msg import Rotorspeed as Msg_Rotorspeed

#Services ROS
from std_srvs.srv import Empty as Srv_Empty

### CONSTANTES =========================================================
DEFAULT_PARAM = 0.0

### CLASSES ============================================================
class Classe(object):
    """
    """

    # Fonctions initialisation -----------------------------------------
    def __init__(self):
        """
        """
        #Recuperation des parametres
        self.param = rospy.get_param("~param", DEFAULT_PARAM)
        #Souscription aux topics ROS
        self.__init__subscribers()
        #Creation des publishers ROS
        self.__init__publishers()

    def __init__publishers(self):
        """
        Initialisation du publisher ROS
        """
        self.pub_rotorspeed = rospy.Publisher("command/rotorspeed", Msg_Rotorspeed, queue_size=1)

    def __init__subscribers(self):
        """
        Initialisation des subscriber ROS
        """
        rospy.Subscriber("topic", Msg_Empty, self.callback_topic)

    # Fonctions publiques ----------------------------------------------
    def run(self):
        """
        """
        #Boucle temporelle
        rate = rospy.Rate(20) # Commande à 20Hz
        while not rospy.is_shutdown():
            rate.sleep()
            self.step()

    def step(self):
        msg_rotorspeed = Msg_Rotorspeed()
        msg_rotorspeed.values = [0.0, 0.0, 0.0, 0.0] #Quadrirotor
        self.pub_rotorspeed.publish(msg_rotorspeed)

    # Fonctions de publication -----------------------------------------

    # Fonctions callback -----------------------------------------------
    def callback_topic(self, data):
        """
        """
        rospy.loginfo("Msg recu")

    # Fonctions privée -------------------------------------------------
    def _reset_world(self):
        """
            self._reset_attributes()
        Intialisations des services
        """
        rospy.wait_for_service('/gazebo/reset_world')
        try:
            reset_world = rospy.ServiceProxy('/gazebo/reset_world', Srv_Empty)
            reset_world()
            self._reset_attributes()
        except rospy.ServiceException, e:
            print "Service reset_world call failed: %s"%e

    def _reset_attributes(self):
        """
        Re-Initialisation des attributs
        """
        return

### FONCTIONS ==========================================================

### MAIN ===============================================================
if __name__=="__main__":
    rospy.init_node("node", anonymous=False)
    node = Classe()
    node.run()
