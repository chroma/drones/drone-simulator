#!/usr/bin/env python 
# coding: utf-8

# Auteur : vledoze
# Date   : 25/05/2018

### IMPORTATIONS =======================================================
#Import lib standard
import math
import numpy
import random
import rospy
import sys
import time

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F

import matplotlib
import matplotlib.pyplot as plt

from collections import namedtuple
from torch.autograd import Variable

#Import lib locale

#Messages ROS
from std_msgs.msg import Empty as Msg_Empty
from std_msgs.msg import Float32 as Msg_Float32
from sensor_msgs.msg import Imu as Msg_Imu
from chroma_gazebo_plugins_msgs.msg import Rotorspeed as Msg_Rotorspeed
from chroma_gazebo_plugins_msgs.msg import BodyGroundTruth as Msg_BodyGroundTruth

#Services ROS
from std_srvs.srv import Empty as Srv_Empty

### CONSTANTES =========================================================
#Constante sur le choix des actions
EPS_START = 1.0
EPS_END = 0.0
EPS_DECAY = 200

#Constante d'apprentissage
GAMMA = 0.999

#Batch size
BATCH_SIZE = 100

#Limite de temps d'un episode
EPISODE_STEPS_LIMIT = 100

#Taille de la memoire
REPLAY_MEMORY = 1000000

#Tuple de la memoire
TRANSITION = namedtuple(
    'Transition',
    ('state', 'action', 'next_state', 'reward'))

#Type de fonction reward
# Au choix : "position" / "gradient (default)"
REWARD_FUNC = "gradient"

#Commandes par defaut
DOWN = 50.0
UP = 85.0
UP_CMD = [UP, UP, UP, UP]
DOWN_CMD = [DOWN, DOWN, DOWN, DOWN]

### CLASSES ============================================================
class ReplayMemory(object):
    """
    Classe qui permet de sauvegarder, d'acceder et de rejouer la memoire
    composée d'episodes "etat, action, etat_suivant, recompense"
    """

    def __init__(self, capacity):
        """
        Initialisation
        """
        self.capacity = capacity #Nombre max d'épisode a memoriser
        self.memory = []
        self.position = 0

    def push(self, *args):
        """
        Sauvegarde un episode. Si la taille maximale de la mémoire est
        atteinte, on écrase les épisodes les plus anciens.
        """
        if len(self.memory) < self.capacity:
            self.memory.append(None)
        self.memory[self.position] = TRANSITION(*args)
        self.position = (self.position + 1) % self.capacity

    def sample(self, batch_size):
        """
        Retourne un episode de la memoire
        """
        return random.sample(self.memory, batch_size)

    def __len__(self):
        """
        Nombre d'épisodes memorisés
        """
        return len(self.memory)

class DQN(nn.Module):
    """ Q-Network """

    def __init__(self):
        """
        Inputs :
            altitude
            imu
            rotorspeed
        Outputs :
            Q(s, add_thrust)
            Q(s, rmv_thrust)
        """
        super(DQN, self).__init__()
        self.fc1 = nn.Linear(3, 10)
        self.fc2 = nn.Linear(10, 50)
        self.fc3 = nn.Linear(50, 10)
        self.head = nn.Linear(10, 2)

    def forward(self, x):
        x = F.leaky_relu(self.fc1(x))
        x = F.leaky_relu(self.fc2(x))
        x = F.leaky_relu(self.fc3(x))
        return F.softmax(self.head(x), dim=1)

class DroneReinforcementLearning(object):
    """
    """

    # Fonctions initialisation -----------------------------------------
    def __init__(self):
        """
        """
        #Attributs apprentissage
        self.__init__learning_attributes()
        #Attributs episodes
        self.__init__episodes_attributes()
        #Creation des services du node
        self.__init__services()
        #Souscription aux topics ROS
        self.__init__subscribers()
        #Creation des publishers ROS
        self.__init__publishers()

    def __init__learning_attributes(self):
        """
        Attributs fixes au cours de l'apprentissage
        """
        #Memoire du drone
        self.memory = ReplayMemory(
            rospy.get_param("~replay_memory", REPLAY_MEMORY))
        #Compteur d'episode
        self.episodes = 0
        #Stockage pour tracés
        self.cumu_reward_list = []
        self.mean_loss_list = []
        #Reseaux
        self.policy_net = DQN().cuda()
        self.target_net = DQN().cuda()
        self.target_net.load_state_dict(self.policy_net.state_dict())
        self.target_net.eval()
        #Optimiser
        self.optimizer = optim.RMSprop(
            self.policy_net.parameters(),
            lr=rospy.get_param("~learning_rate", 0.01))
        #batch size
        self.batch_size = rospy.get_param("~batch_size", BATCH_SIZE)
        #Choix fonction reward
        self.reward_func = rospy.get_param("~reward_func", REWARD_FUNC)
        #Methode de calcul du cout
        self.loss_method = "method_2"

    def __init__episodes_attributes(self):
        """
        Attributs qui seront remis à zero à chaque nouvel épisode
        """
        #Indicateur drone OK
        self.drone_ok = True
        #Indicateur apprentissage OK
        self.learning_ok = True
        #Etat initial
        self.altitude = 0.0126
        self.verticalspeed = 0.0
        self.rotorspeed = 0.0
        self.observations = torch.FloatTensor([[
            self.altitude,
            self.verticalspeed,
            self.rotorspeed]])
        #Nombre de pas de temps par episode
        self.episode_steps = 0
        #Action en cours
        self.action = torch.LongTensor([[0]])
        #Recompense
        self.reward = torch.FloatTensor([[0.0]])
        #Recompense cumulée
        self.cumu_reward = 0.0
        #Cout
        self.loss_list = []
        #Nombre d'épisodes
        self.episodes += 1
        #
        self.has_no_trace = True

    def __init__publishers(self):
        """
        Initialisation du publisher ROS
        """
        self.pub_rotorspeed = rospy.Publisher(
            "command/rotorspeed", Msg_Rotorspeed, queue_size=10)

    def __init__subscribers(self):
        """
        Initialisation des subscriber ROS
        """
        rospy.Subscriber("imu", Msg_Imu, self.callback_imu)
        rospy.Subscriber(
            "groundtruth", Msg_BodyGroundTruth, self.callback_groundtruth)
        rospy.Subscriber(
            "rotorspeed/0", Msg_Float32, self.callback_rotorspeed_0)
        rospy.Subscriber('start_learning', Msg_Empty, self.callback_start_learning)
        rospy.Subscriber('stop_learning', Msg_Empty, self.callback_stop_learning)

    def __init__services(self):
        """
        Initialisation des services
        """

    # Fonctions publiques ----------------------------------------------
    def run(self):
        """
        """
        #Boucle temporelle
        rate = rospy.Rate(rospy.get_param("~freq", 10.0)) # Commande à 10Hz
        while not rospy.is_shutdown():
            rate.sleep()
            self.step()

    def step(self):
        """
        Step d'apprentissage
        """
        #Probleme
        if self.drone_ok & self.learning_ok:
            #Vecteur observations
            observations = torch.FloatTensor([[
                self.altitude,
                self.verticalspeed,
                self.rotorspeed]])
            #Fonction de reward
            if (self.reward_func == "position"):
                self.reward_function_position(observations)
            else:
                self.reward_function_gradient(observations)
            #Action
            self.select_action(observations)
            #Envoi des commandes aux moteurs
            msg_rotorspeed = Msg_Rotorspeed()
            msg_rotorspeed.values = DOWN_CMD if (int(self.action)==0) \
                else UP_CMD #Vitesse de rotation des 4 moteurs (rad/s)
            self.pub_rotorspeed.publish(msg_rotorspeed)
        #Tracé si besoin
        if (self.episodes > 1) & (self.has_no_trace):
            #On trace les courbes loss moyen et reward cumulé
            plt.clf()
            plt.title('Training...')
            ax = plt.subplot(111)
            ax.plot(self.cumu_reward_list, label="cumulative_reward", color="blue")
            ax.set_xlabel('Episode')
            ax.set_ylabel('reward')
            ax.legend(loc=0)
            ax1 = ax.twinx()
            ax1.set_ylabel('loss')
            ax1.plot(self.mean_loss_list, label="mean_loss", color="green")
            ax1.legend(loc=1)
            plt.pause(0.001)  # pause a bit so that plots are updated
            #
            self.has_no_trace = False

    def select_action(self, observations):
        """
        Choisi une action en fonction d'une observations
        Actions = Vitesses de rotation des 4 moteurs (rad/s)
        """
        #Store the transition in memory
        self.memory.push(self.observations, self.action, observations, self.reward)
        # Perform one step of the optimization (on the target network)
        self.optimize_model()
        #Conditions to use the policy net
        trigger = random.random()
        threshold = EPS_END + (EPS_START - EPS_END) * \
            math.exp(-1. * self.episodes / EPS_DECAY)
        if trigger > threshold:
            self.action = self.policy_net(Variable(observations, volatile=True).cuda()).data.max(1)[1].view(1, 1).cpu()
        #Otherwise choose random actions
        else:
            self.action = torch.LongTensor([[random.randrange(2)]])
        #Memorisation de l'état
        self.observations = observations

    def reward_function_position(self, observations):
        """
        Calcul un reward en fonction de l'état courant
        """
        if observations[0][0] < 0.1:
            reward = 0.0
        else:
            ecart = observations[0][0] - 1.0 #On vise un vol à altitude constante = 1.0m
            ecart = abs(ecart*10.0)
            # ecart = min(ecart, 1.0) #On borne l'écart à 1.0m
            reward = 1.0/max(ecart, 1.0) #Reward important si ecart faible. Pas de division par 0
        self.reward = torch.FloatTensor([[reward]])
        self.cumu_reward += math.pow(GAMMA, self.episode_steps)*reward
        self.episode_steps += 1

    def reward_function_gradient(self, observations):
        """
        Calcul un reward en fonction de l'état courant
        """
        dist_cur = abs(observations[0][0] - 1.0)
        dist_old = abs(self.observations[0][0] - 1.0)
        if dist_old > (dist_cur + 0.01):
            reward = 1.0
        else:
            reward = 0.0
        self.reward = torch.FloatTensor([[reward]])
        self.cumu_reward += math.pow(GAMMA, self.episode_steps)*reward
        self.episode_steps += 1

    def optimize_model(self):
        #Optimisation seulement si suffisamment de memoire
        if len(self.memory) < self.batch_size:
            return
        #Recuperation de n-transitions
        transitions = self.memory.sample(self.batch_size)
        # Transpose the batch
        # (see http://stackoverflow.com/a/19343/3343043 for detailed explanation).
        batch = TRANSITION(*zip(*transitions))
        state_batch = Variable(torch.cat(batch.state)).cuda()
        action_batch = Variable(torch.cat(batch.action)).cuda()
        next_state_batch = Variable(torch.cat(batch.next_state)).cuda()
        reward_batch = Variable(torch.cat(batch.reward)).cuda()
        #Compute the Loss
        if (self.loss_method == "Bellman"):
            # Compute Q(s_t, a)
            # The model computes Q(s_t), then we select the columns of actions taken
            state_action_values = self.policy_net(state_batch).gather(1, action_batch.long())
            # Compute V(s_{t+1}) for all next states.
            next_state_value = self.target_net(next_state_batch).max(1)[0].view(self.batch_size, 1)
            # Compute the expected Q values
            expected_state_action_values = Variable(((next_state_value * GAMMA) + reward_batch).data)
            # Compute Huber loss
            loss = F.smooth_l1_loss(state_action_values, expected_state_action_values)# # Compute Q(s_t, a)
        elif (self.loss_method == "method_1"):
            # The model computes Q(s_t), then we select the columns of actions taken
            state_action_values = self.policy_net(state_batch).gather(1, action_batch.long())
            # Loss = Q(s,t) - R(s, a)
            loss = F.smooth_l1_loss(state_action_values, reward_batch)
        else:
            # A(s, t), vecteur des probabilités d'actions
            policy_output = self.policy_net(state_batch)
            #R_bar : inverse des reward (si r=1, r_bar=0)
            reward_bar = torch.abs(reward_batch - 1.0)
            #Sortie attendue = 1 pour la meilleur action et 0 sinon
            expected_output = torch.cat([reward_bar, reward_bar], dim=1)
            for i in range(self.batch_size):
                expected_output[i, action_batch.long().data[i][0]] = reward_batch[i, 0]
            # Loss = A(s, t) - R(s, a)
            loss = F.l1_loss(policy_output, expected_output)
        self.loss_list.append(float(loss.data))
        # Optimize the model
        self.optimizer.zero_grad()
        loss.backward()
        for param in self.policy_net.parameters():
            param.grad.data.clamp_(-1, 1)
        self.optimizer.step()

    # Fonctions de publication -----------------------------------------

    # Fonctions callback -----------------------------------------------
    def callback_rotorspeed_0(self, rotor_data):
        """
        Recuperation de la vitesse des rotors
        """
        self.rotorspeed = rotor_data.data

    def callback_imu(self, imu_data):
        """
        Recuperation du message IMU
        Structure du message : http://docs.ros.org/api/sensor_msgs/html/msg/Imu.html
        """
        if ((imu_data.linear_acceleration.z < 0.0) | \
           (imu_data.linear_acceleration.z > 1000.0)) & \
           (self.drone_ok == True):
           #Crash probable du drone : on arrete l'apprentissage
           self.drone_ok = False

    def callback_groundtruth(self, gt_data):
        """
        Recuperation de la vérité terrain
        Structure du message : chroma_gazebo_plugins_msgs/msg/BodyGroundTruth
        Fonction appelée à chaque pas de temps de la simulation Gazebo
        """
        self.altitude = gt_data.z_enu
        self.verticalspeed = gt_data.vz_enu

    # Fonctions privée -------------------------------------------------
    def callback_stop_learning(self, req):
        """
        fonction appelée par le service stop_learning
        """
        #On met l'apprentissage en stand-by
        self.learning_ok = False
        #On envoie des commandes nulles pour arreter les rotors
        msg_rotorspeed = Msg_Rotorspeed()
        msg_rotorspeed.values = [0.0, 0.0, 0.0, 0.0] #Vitesse de rotation des 4 moteurs (rad/s)
        self.pub_rotorspeed.publish(msg_rotorspeed)
        #Reward moyen
        self.cumu_reward_list.append(self.cumu_reward)
        #Loss moyen
        if len(self.loss_list) > 0:
            self.mean_loss_list.append(numpy.mean(self.loss_list))
        else:
            self.mean_loss_list.append(1.0)

    def callback_start_learning(self, req):
        """
        fonction appelée par le service start_learning
        """
        #Reset des attributs
        self.__init__episodes_attributes()
        self.target_net.load_state_dict(self.policy_net.state_dict())
        #On relance l'apprentissage
        self.learning_ok = True
        return Srv_Empty()

### FONCTIONS ==========================================================

### MAIN ===============================================================
if __name__=="__main__":
    rospy.init_node("node", anonymous=False)
    node = DroneReinforcementLearning()
    plt.figure()
    node.run()
