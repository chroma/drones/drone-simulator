#!/bin/bash

if [ $# = "0" ]; then
  echo "Arguments :"
  echo "1 = Chemin absolu du dossier git <Firmware px4> "
  echo "2 = Chemin absolu du dossier git <drone_simulator>"
  exit 0
fi

if [ $1 = "-h" ]; then
  echo "Arguments :"
  echo "1 = Chemin absolu du dossier git <Firmware px4> "
  echo "2 = Chemin absolu du dossier git <drone_simulator>"
  exit 0
fi

if [ $# = "2" ]; then
  x0=0.0
  y0=0.0
  z0=5.01
  # z0=0.0
  psi0_deg=70.0
  psi0_rad=`echo "$psi0_deg * 3.14 / 180.0" | bc -l`
  xterm -e "cd $1; ./build/posix_sitl_default/px4 $2/ROS_quadrotor_simulator/SITL/intelaero.config" &
  # xterm -e "roslaunch chroma_gazebo_models drone_world.launch drone:=intelaero_mod world:=empty_world x:=$x0 y:=$y0 z:=$z0 yaw:=$psi0_rad; pkill -9 gzclient; pkill -9 gzserver" &
  xterm -e "roslaunch chroma_gazebo_models drone_world.launch drone:=intelaero_mod world:=city x:=$x0 y:=$y0 z:=$z0 yaw:=$psi0_rad; pkill -9 gzclient; pkill -9 gzserver" &
  sleep 10.0
  roslaunch chroma_gazebo_interfaces mavros_intelaero.launch
  pkill -9 gzclient
  pkill -9 gzserver
else
  echo "Nombre incorrect d'arguments... "
  exit 0
fi
